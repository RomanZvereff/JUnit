package com.zrv.primenumber;

import java.util.ArrayList;
import java.util.List;

public class PrimeNumbers {

    public List<Integer> betweenMimMaxNumbers(String min, String max) {

        List<Integer> arrayOfNum = new ArrayList<>();

        String min1 = min;
        int minNumber = Integer.parseInt(min1);

        String max1 = max;
        int maxNumber = Integer.parseInt(max1);

        while ((minNumber + 1) < maxNumber) {
            for (int i = minNumber; i < (maxNumber - 1); i++) {
                int betweenMinMax = ++minNumber;
                arrayOfNum.add(betweenMinMax);
            }
        }
        return arrayOfNum;
    }
}