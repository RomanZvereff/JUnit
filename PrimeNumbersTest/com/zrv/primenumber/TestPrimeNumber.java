package com.zrv.primenumber;

import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;

public class TestPrimeNumber {

    PrimeNumbers primeNumbers = new PrimeNumbers();

    @Test(expected = IllegalArgumentException.class)
    public void testAddArgument(){
        primeNumbers.betweenMimMaxNumbers("1", "X");
    }

    @Test(expected = NumberFormatException.class)
    public void testAddNull(){
        primeNumbers.betweenMimMaxNumbers(" ", "10");
    }

    @Test
    public void testRightPlace(){
        List<Integer> primeNum = primeNumbers.betweenMimMaxNumbers("1", "10");
        assertEquals(0, primeNum.indexOf(2));
        assertEquals(7, primeNum.indexOf(9));
    }
}